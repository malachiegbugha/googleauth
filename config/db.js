const mongoose = require('mongoose')
const connectDB = async () =>{
    try{

        const conn = await mongoose.connect(`mongodb://localhost/authdb`,{
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        console.log(`Mongodb connected: ${conn.connection.host} `);
    }catch(err){
        console.error(err);
        process.exit(1)

    }
}

module.exports = connectDB;
